# This is an example how to develope with React and Docker

## Install

1. Clone the repo into a directory you created before

2. Run `npm install`

## Handle with the Dockers

3. After that we create a docker container with the bash-script `dimagecreate`. It accepts two arguments: name of the image and mode (developement or production). e.g. `./dimagecreate imagename dev` or `./dimagecreate imagename prod` If you only run `./dimagecreate` without arguments it will ask you to give a name and the mode (dev or prod).

4. If you want to develop use `code .` to start VS Code in the directory of this project.

5. To start a docker container in *developing* mode first ensure that you have build a docker image before in dev mode. Then you can use the bash-script `dcontainer`. It accepts two arguments: name of the image (you created before) and mode (developement or production). So in this case we use `./dcontainer imagename dev`. If you only run `./dcontainer` without arguments it will ask you to give a name and the mode (dev or prod).

6. After the container was created you find the app via http://localhost:3000

7. Now you can start developing in src/ via your opened VS Code. You can see your changes immediatly (it's the same as without a docker container).

8. To start a docker container in *production* mode first ensure that you have build a docker image before in prod mode. Then you can use the bash-script `dcontainer`. It accepts two arguments: name of the image (you created before) and mode (developement or production). So in this case we use `./dcontainer imagename prod`. If you only run `./dcontainer` without arguments it will ask you to give a name and the mode (dev or prod).

9. After the container was created you find the app via http://localhost:80

## The magic in the bash scipts

Docker Images for developing mode will be created with the command `sudo docker build -t $dockername .`. It automaticly creates a docker image with ...:latest. See `sudo docker images`. 

Docker Images for production mode will be created with the command `sudo docker build -f Dockerfile.prod -t $dockername:prod .`. It automaticly creates a docker image with ...:prod. See `sudo docker images`. 

Docker Containers for production mode will be created with the command `sudo docker run -it --rm -v ${PWD}:/app -p 3000:3000 -e CHOKIDAR_USEPOLLING=true $dockername`. 

Docker Containers for production mode will be created with the command `sudo docker run -it --rm -p 80:80 $dockername:prod`. 

## Source
Based on https://mherman.org/blog/dockerizing-a-react-app/